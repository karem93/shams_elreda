<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\FaqResource;
use App\Http\Resources\PageResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\SettingResource;
use App\Models\Article;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Faq;
use App\Models\Page;
use App\Models\Setting;
use Illuminate\Http\Request;


class HomeController extends Controller
{

    public function getCategories()
    {
        $categories = Category::all();
        return $this->successResponse(CategoryResource::collection($categories));
    }


    public function getProducts($id)
    {
        $products = Category::find($id)->articles()->paginate(5);
        return $this->paginateResponse(ProductResource::collection($products), $products);

    }

    public function showProduct($id)
    {
        $product = Article::find($id);
        return $this->successResponse(ProductResource::make($product));
    }


    public function getPage($slug)
    {
        $page = Page::where('slug', $slug)->first();
        return $this->successResponse(PageResource::make($page));
    }


    public function getSettings()
    {
        $keys = ['website_name', 'website_bio', 'address', 'contact_email', 'facebook_link', 'twitter_link', 'instagram_link', 'phone', 'phone2', 'whatsapp_phone'];
        $settings = Setting::whereIn('key', $keys)->get();

       $data = [];
       foreach ($settings as $setting) {
           $data[$setting->key] = $setting->value;
       }
        return $this->successResponse($data);
    }

    public function contactUs(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|string',
            'message' => 'required'
        ]);

        Contact::create($data);
        return $this->successResponse(message: __('lang.contact_sent'));
    }
}
