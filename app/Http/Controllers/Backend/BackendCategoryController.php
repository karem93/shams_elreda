<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class BackendCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:categories-create', ['only' => ['create','store']]);
        $this->middleware('can:categories-read',   ['only' => ['show', 'index']]);
        $this->middleware('can:categories-update',   ['only' => ['edit','update']]);
        $this->middleware('can:categories-delete',   ['only' => ['delete']]);
    }


    public function index(Request $request)
    {
        $categories =  Category::where(function($q)use($request){
            if($request->id!=null)
                $q->where('id',$request->id);
            if($request->q!=null)
                $q->where('title','LIKE','%'.$request->q.'%')->orWhere('description','LIKE','%'.$request->q.'%');
        })->withCount(['articles'])->orderBy('id','DESC')->paginate();

        return view('admin.categories.index',compact('categories'));
    }


    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        $request->merge([
            'slug'=>\MainHelper::slug($request->slug)
        ]);

        $request->validate([
            'slug'=>"required|max:190|unique:categories,slug",
            'title'=>"required|max:190",
            'title_en'=>"required|max:190",
            'description'=>"nullable|max:999999",
            'description_en'=>"nullable|max:999999",
            'meta_description'=>"nullable|max:999999",
            'meta_description_en'=>"nullable|max:999999",
        ]);
        $category = Category::create([
            'user_id'=>auth()->user()->id,
            "slug"=>$request->slug,
            "title"=>$request->title,
            "title_en"=>$request->title_en,
            "description"=>$request->description,
            "description_en"=>$request->description_en,
            "meta_description"=>$request->meta_description,
            "meta_description_en"=>$request->meta_description_en,
        ]);
        if($request->hasFile('image')){
            $image = $category->addMedia($request->image)->toMediaCollection('image');
            $category->update(['image'=>$image->id.'/'.$image->file_name]);
        }
        \MainHelper::move_media_to_model_by_id($request->temp_file_selector,$category,"description");
        toastr()->success(__('utils/toastr.successful_process_message'));
        return redirect()->route('admin.categories.index');
    }


    public function show(Category $category)
    {
    }


    public function edit(Category $category)
    {
        return view('admin.categories.edit',compact('category'));
    }


    public function update(Request $request, Category $category)
    {
        $request->merge([
            'slug'=>\MainHelper::slug($request->slug)
        ]);

        $request->validate([
            'slug'=>"required|max:190|unique:categories,slug,".$category->id,
            'title'=>"required|max:190|unique:categories,title,".$category->id,
            'title_en'=>"required|max:190|unique:categories,title_en,".$category->id,
            'description'=>"nullable|max:999999",
            'description_en'=>"nullable|max:999999",
            'meta_description'=>"nullable|max:999999",
            'meta_description_en'=>"nullable|max:999999",
        ]);
        $category->update([
            "slug"=>$request->slug,
            "title"=>$request->title,
            "title_en"=>$request->title_en,
            "description"=>$request->description,
            "description_en"=>$request->description_en,
            "meta_description"=>$request->meta_description,
            "meta_description_en"=>$request->meta_description_en,
        ]);
        if($request->hasFile('image')){
            $image = $category->addMedia($request->image)->toMediaCollection('image');
            $category->update(['image'=>$image->id.'/'.$image->file_name]);
        }
        \MainHelper::move_media_to_model_by_id($request->temp_file_selector,$category,"description");
        toastr()->success(__('utils/toastr.successful_process_message'));
        return redirect()->route('admin.categories.index');
    }


    public function destroy(Category $category)
    {
        $category->delete();
        toastr()->success(__('utils/toastr.successful_process_message'));
        return redirect()->route('admin.categories.index');
    }
}
