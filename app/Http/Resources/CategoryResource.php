<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => app()->getLocale() == 'ar' ? $this->title : $this->title_en,
            'description' => app()->getLocale() == 'ar' ? $this->description : $this->description_en,
            'meta_description' => app()->getLocale() == 'ar' ? $this->meta_description : $this->meta_description_en,
            'slug' => $this->slug,
            'image' => url($this->image('thumb')),
        ];
    }
}
