<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => app()->getLocale() == 'ar' ? $this->title : $this->title_en,
            'description' => app()->getLocale() == 'ar' ? $this->description : $this->description_en,
            'meta_description' => app()->getLocale() == 'ar' ? $this->meta_description : $this->meta_description_en,
            'image' => url($this->main_image('thumb')),
            'is_featured' => (boolean)$this->is_featured,
            'category' => CategoryResource::make(collect($this->categories)->first()),
            'views' => $this->views
        ];
    }
}
