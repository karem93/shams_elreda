<!doctype html>
<html lang="ar" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('seo.index')
    {!!$settings['header_code']!!}
    @livewireStyles
    @if(auth()->check())
        @php
            if(session('seen_notifications')==null)
                session(['seen_notifications'=>0]);
            $notifications=auth()->user()->notifications()->orderBy('created_at','DESC')->limit(50)->get();
            $unreadNotifications=auth()->user()->unreadNotifications()->count();
        @endphp
    @endif
    @vite('resources/css/app.css')
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <style type="text/css">
        body {
            --bg-main: #fff;
            --bg-second: #f4f4f4;
            --font-1: #333333;
            --font-2: #555555;
            --border-color: #dddddd;
            --main-color: #0194fe;
            --main-color-rgb: 1, 148, 254;
            --main-color-flexable: #0194fe;
            --scroll-bar-color: #d1d1d1;
        }

        body.night {
            --bg-main: #1c222b;
            --bg-second: #131923;
            --font-1: #fff;
            --font-2: #e3e3e3;
            --border-color: #33343b;
            --main-color: #0194fe;
            --main-color-rgb: 1, 148, 254;
            --main-color-flexable: #15202b;
            --scroll-bar-color: #505050;
        }

    </style>
    @yield('styles')
</head>
<body style="background:#eef4f5;margin-top: 65px;" class="body">
<style type="text/css">
    #toast-container > div {
        opacity: 1;
    }
</style>
@yield('after-body')
<div id="app">
    {{-- <div class="page-loader"></div> --}}
    <div id="body-overlay"
         onclick="document.getElementById('aside-menu').classList.toggle('active');document.getElementById('body-overlay').classList.toggle('active');"></div>
{{--    <x-navbar/>--}}
    <main class="p-0 font-2">

        <style type="text/css">
            #navbar {
                display: none;
            }

            .form-control.is-invalid, .was-validated .form-control:invalid {
                border-color: #dc3545 !important;
                background-color: rgb(255 184 184 / 41%) !important;
                padding-left: calc(1.5em + 0.75rem);
                background-image: url(data:image/svg+xml,%3csvg xmlns= 'http://www.w3.org/2000/svg' viewBox= '0 0 12 12' width= '12' height= '12' fill= 'none' stroke= '%23dc3545' %3e%3ccircle cx= '6' cy= '6' r= '4.5' /%3e%3cpath stroke-linejoin= 'round' d= 'M5.8 3.6h.4L6 6.5z' /%3e%3ccircle cx= '6' cy= '8.2' r= '.6' fill= '%23dc3545' stroke= 'none' /%3e%3c/svg%3e) !important;
                background-repeat: no-repeat;
                background-position: left calc(0.375em + 0.1875rem) center;
                background-size: calc(0.75em + 0.375rem) calc(0.75em + 0.375rem);
            }

            body {
                background: #fff !important;
            }
        </style>

        <div class="container p-0">
            <div class="col-12 p-0 row d-flex">
                <div class="col-12 col-lg-6 text-center p-0" style="">
                    <div class="col-12 p-2 p-lg-4 align-items-center justify-content-center d-flex row"
                         style="min-height:70vh">
                        <div class="col-12 p-3 p-lg-4" style="background:#fff;border-radius: 10px;">
                            <form method="POST" action="{{ route('login') }}" class="row m-0">
                                @csrf

                                <div class="col-12 p-0 mb-5 mt-3"
                                     style="width: 550px;max-width: 100%;margin: 0px auto;">
                                    <h3 class="mb-4 font-4">{{ __('lang.login') }}</h3>

                                </div>
                                <div class="nafezly-divider-right"
                                     style="    background-image: linear-gradient( 90deg,transparent,rgb(0 0 0/72%));right: auto;left: 10px;opacity: .1;margin: 14px 0;min-height: 2px;"></div>


                                <div class="form-group row mb-4 col-12 col-lg-6   px-0 pt-2 ">
                                    <div class="col-md-12 px-2 pt-4" style="position: relative;">
                                        <label for="email"
                                               class="col-form-label text-md-right mb-1 font-small px-2 py-1 d-inline"
                                               style="background:#f4f4f4;position: absolute;top: 17px;right: 20px; border-radius: 3px!important">البريد
                                            الالكتروني</label>
                                        <input id="email" type="email"
                                               class="form-control mt-2 d-inline-block @error('email') is-invalid @enderror"
                                               name="email" value="" required="" autocomplete="off" autofocus=""
                                               style=";height: 42px;border-color: #eaedf1;border-radius: 3px!important"
                                               value="{{ old('email') }}">
                                    </div>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>

                                <div class="form-group row mb-4 col-12 col-lg-6   px-0 pt-2 ">
                                    <div class="col-md-12 px-2 pt-4" style="position: relative;">
                                        <label for="password"
                                               class="col-form-label text-md-right mb-1 font-small px-2 py-1 d-inline"
                                               style="background:#f4f4f4;position: absolute;top: 17px;right: 20px;border-radius: 3px!important">كلمة
                                            المرور</label>
                                        <input id="password" type="password"
                                               class="form-control mt-2 d-inline-block @error('password') is-invalid @enderror"
                                               name="password" value="" required="" autocomplete="off" autofocus=""
                                               style=";height: 42px;border-color: #eaedf1;border-radius: 3px!important"
                                               minlength="6" aria-invalid="true">
                                    </div>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                                <div class="col-12 p-0 row d-flex align-items-center">
                                    <div class="col-12 col-lg-6 p-2">
                                        <div class="form-group row ">
                                            <div class="col-12 p-0">
                                                <input class="form-check-input ms-2 me-0" type="checkbox"
                                                       name="remember" id="remember"
                                                       {{ old('remember')||old('remember')==null ? 'checked' : '' }} style="position:relative;height: 20px;width: 20px;cursor: pointer;">

                                                <label class="form-check-label" for="remember"
                                                       style="position:relative;cursor: pointer;">
                                                    {{ __('lang.remember_me') }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6 p-2 ">
                                        <div class="form-group row mb-0 ">
                                            <div class="col-12 p-0 d-flex justify-content-lg-end">
                                                <button type="submit" class="btn btn-success font-1">
                                                    {{ __('lang.login') }}
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div
                    class="col-12 col-lg-6 d-none d-lg-flex text-center p-0 d-flex align-items-center justify-content-center row position-relative"
                    style="">
                    <div class="overlap-grid overlap-grid-2">
                        <div class="item mx-auto">
                            <div class="shape bg-dot primary rellax w-16 h-20" data-rellax-speed="1"
                                 style="top: 3rem; left: 5.5rem"></div>
                            <div class="col-12 p-0 align-items-center py-5 justify-content-center d-flex svg-animation"
                                 style="background-image: url('{{$settings['get_website_logo']}}');background-size: cover;padding-top: 57%;background-position: center;height: 342px;z-index: 1;position: relative;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>
{{--    <x-footer/>--}}
</div>


@vite('resources/js/app.js')
@livewireScripts
@include('layouts.scripts')
@auth
    <script type="module">
        var favicon = new Favico({
            bgColor: '#dc0000',
            textColor: '#fff',
            animation: 'slide',
            fontStyle: 'bold',
            fontFamily: 'sans',
            type: 'circle'
        });

        function get_website_title() {
            return $('meta[name="title"]').attr('content');
        }

        var notificationDropdown = document.getElementById('notificationDropdown')
        notificationDropdown.addEventListener('show.bs.dropdown', function () {
            $.ajax({
                method: "POST",
                url: "{{route('admin.notifications.see')}}",
                data: {_token: "{{csrf_token()}}"}
            }).done(function (res) {
                $('#dropdown-notifications-icon').fadeOut();
                favicon.badge(0);
            });
        });

        function append_notification_notifications(msg) {
            if (msg.count_unseen_notifications > 0) {
                $('#dropdown-notifications-icon').fadeIn(0);
                $('#dropdown-notifications-icon').text(msg.count_unseen_notifications);
            } else {
                $('#dropdown-notifications-icon').fadeOut(0);
                favicon.badge(0);
            }
            $('.notifications-container').empty();
            $('.notifications-container').append(msg.response);
            $('.notifications-container a').on('click', function () {
                window.location.href = $(this).attr('href');
            });
        }

        function get_notifications() {
            $.ajax({
                method: "GET",
                url: "{{route('admin.notifications.ajax')}}",
                success: function (data, textStatus, xhr) {

                    favicon.badge(data.notifications.response.count_unseen_notifications);

                    if (data.alert) {
                        var audio = new Audio('{{asset("/sounds/notification.wav")}}');
                        audio.play();
                    }
                    append_notification_notifications(data.notifications.response);
                    if (data.notifications.response.count_unseen_notifications > 0) {
                        $('title').text('(' + parseInt(data.notifications.response.count_unseen_notifications) + ')' + " " +
                            get_website_title());

                    } else {
                        $('title').text(get_website_title());
                    }
                }
            });
        }

        window.focused = 25000;
        window.onfocus = function () {
            get_notifications();
            window.focused = 25000;
        };
        window.onblur = function () {
            window.focused = 60000;
        };

        function get_nots() {
            setTimeout(function () {
                get_notifications();
                get_nots();
            }, window.focused);
        }

        get_nots();

        @if($unreadNotifications!=session('seen_notifications') && $unreadNotifications!=0)
        @php
            session(['seen_notifications'=>$unreadNotifications]);
        @endphp
        var audio = new Audio('{{asset("/sounds/notification.wav")}}');
        audio.play();
        @endif
    </script>
@endauth
@yield('scripts')
{!!$settings['footer_code']!!}
</body>
</html>
