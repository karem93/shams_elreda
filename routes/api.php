<?php

use App\Http\Controllers\Api\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'site','middleware' => 'appLocale'], function () {
    Route::get('categories', [HomeController::class, 'getCategories']);
    Route::get('products/{category_id}', [HomeController::class, 'getProducts']);
    Route::get('product/{id}', [HomeController::class, 'showProduct']);
    Route::get('settings', [HomeController::class, 'getSettings']);
    Route::get('get_page/{slug}', [HomeController::class, 'getPage']);
    Route::post('contact_us', [HomeController::class, 'contactUs']);
});
